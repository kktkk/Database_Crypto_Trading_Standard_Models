<h2>Introduction</h2>
The Standardization of SQAlchemy Models for Programmation in Python and other programming languages is starting to seem a necessity as the handling of data from the different APIs is getting very "painful" difficult.
An example of how the standard is already in place because of the intrinsic properties of the object requested is the ticker data, e.g.:  'open', 'close', 'high', 'low', 'volume'. This dataset is easily  transposable to any type of model/table schema as the important informations are often (99.99%) returned in numerical values that can be easily be inserted to any sqlalchemy data model/table schema.
<h2>Data Targets</h2>
The data targets that are going to be referred and analyzed will be the data provided by the Exchanges in the 2  market types: Exchange/Margin Exchange and the Main Ledger/Balance. A standard model for the User object has also been included for ease of re-use.
An example of the datasets targets can be found bellow. These  are Balance datasets examples from the exchanges Bitfinex and Bittrex.
<h4>Balance examples:</h4>
<h5>Bitfinex</h5>
<code>[{
  "currency":"USD",
  "amount":"-246.94",
  "balance":"515.4476526",
  "description":"Position claimed @ 245.2 on wallet trading",
  "timestamp":"1444277602.0"
}]</code>

<h5>Bittrex</h5>
<code>{
	"success" : true,
	"message" : "",
	"result" : [{
			"PaymentUuid" : "554ec664-8842-4fe9-b491-06225becbd59",
			"Currency" : "BTC",
			"Amount" : 0.00156121,
			"Address" : "1K37yQZaGrPKNTZ5KNP792xw8f7XbXxetE",
			"Opened" : "2014-07-11T03:41:25.323",
			"Authorized" : true,
			"PendingPayment" : false,
			"TxCost" : 0.00020000,
			"TxId" : "70cf6fdccb9bd38e1a930e13e4ae6299d678ed6902da710fa3cc8d164f9be126",
			"Canceled" : false,
			"InvalidAddress" : false
		}, {
			"PaymentUuid" : "d3fdf168-3d8e-40b6-8fe4-f46e2a7035ea",
			"Currency" : "BTC",
			"Amount" : 0.11800000,
			"Address" : "1Mrcar6715hjds34pdXuLqXcju6QgwHA31",
			"O
			pened" : "2014-07-03T20:27:07.163",
			"Authorized" : true,
			"PendingPayment" : false,
			"TxCost" : 0.00020000,
			"TxId" : "3efd41b3a051433a888eed3ecc174c1d025a5e2b486eb418eaaec5efddda22de",
			"Canceled" : false,
			"InvalidAddress" : false
		}
    ]
}</code>
<h2>The Problem</h2>
As we can see in the code above the datasets are clearly not standardized. Obviously required key:values are necessary for the representation of the data like amount, currency/coin, timestamp of transaction, transaction unique identification.. The problems resides on the architecture of such data representation, as we can get a variety of different architectures requiring more or less keys:values per dataset such as descriptions, directions/type, etc.  Such problem is clearly felt when dealing with these datasets types. This generally require an adaptation of the modules or the creation of wrappers for the API calls that will them do the conversion/filtering of the datasets keys:values.  Such problem would be avoided if a standardization of the datasets could be implemented, therefore I present my proposal in the hope that exchanges can think more on developers when creating their own API routes.
<h2>The Proposal</h2>
It simply consists in Base Models representing the 2 datasets types referred above and a set of call examples that could be made to an exchange API (<strong>WIP</strong>).
Base Models should be limited to a strict minimum amount of data/keys:values that should be received in a single call to the API exchange. This should be clear of long strings and should ideally only represent a single direction in a single market, or as you will seen in the code allow a concatenation of the requests that could then be reconstructed in different models but always respecting the hierarchy of essential data, to what can be considered noise. 
The concatenation method can be simply and more efficiently implemented with serialization/hashing of url request/handlers theses would result in hashes that could also reduce headers size by/if integrating user-token system into the hash algorithm.
Obviously this will create an overload on sever-side for the decryption involved, but in the era of user experience such effort is necessary from the exchanges, IMHO..
Let me know what you think of this simple idea..

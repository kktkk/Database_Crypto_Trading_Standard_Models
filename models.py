#!/usr/env python

import hamc
import logging
from exceptions import Exception
from datetime import datetime
from sqlalchemy import SQLAlchemy
from sqlalchemy.sqla.models import MdelView, BaseView
from sqlalchemy.sqla import Column , String, Integer, Float, Boolean, DateTime, ForeignKey
from sqlalchemy.sqla import create_engine, relationship

SECRET = ''


class User(Base):
    __tablemname__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(String(255))
    pwd = Column(String(255))
    email = Column(String(255))
    email_verified = Column(Boolean)
    email_verified_since = Column(DateTime)
    registered = Column(Datetime)
    is_live = Column(Boolean, default=False) #for session handling in database/filesystem
    token = Column(String(255))
    role = Column(String(255), default='USER') 
  
    def __initi__(self, username, pwd, email):
        self.username = username
        self.pwd = gen_pwd(password)
        self.email = email
        self.registered = datetime.utc_now()
        
    pwd_counter = 0
    
    def gen_pwd(self, passsword):
        hash = hmac.new(SECRET, password).hexdisgest()
        return hash
    
    def check_pwd(self, password):
        if pwd_counter => 3:
            logging.warning('Pwd_counter exceeded')
            return False
        hash = hmac.new(SECRET, password).hexdisgest()
        if self.pwd == hash:
            return True
        logging.warning('%s entered wrong password' % self.username)
            pwd_counter += 1
            reurn False
        
    def save(self):
        try:
            db.add(self)
            db.commit()
        except Exception as e:
            logging.debug(e)
            db.rollback()
            
    def get_id(self):
        return int(self.id)
        
    def __repr__(self):
        return '%s' % self.username
        
    def get_token(self):
        return '%s' % self.token
        
    def get_role(self):
        return '%s' % self.role        
        
class Balance(Base):
    __tablename__ = 'balance'
    id = Column(Integer, primary_key=True)
    currency = Column(String(255))
    tx_dir = Column(String(255)) #Need to set choices: deposit/withdrawal
    tx_id = Column(String(255))
    amount = Column(Float)
    balance = Column(Float, default=0.0)
    timestamp = Column(DateTime)
    userId = Column(ForeignKey, 'user.id')
    
    def __init__(self, currency, tx_dir, tx_id, amount, userId):
        self.currency, self.tx_dir, self.tx_id, self.amount = currency, tx_dir, tx_id, amount
        self.userId = userId #check if adding a relationship on instantiation would work
        
    def get_balance(self, currency):
        if self.currency == currency:
            self.order_by('timestamp').first()
            return Float(self.balance)
            
     def save(self):
        try:
            db.add(self)
            db.commit()
        except Exception as e:
            logging.debug(e)
            db.rollback()
            
            
  class TxHistory(Base):
    __tablename__ = 'tx_history'
    id = Column(Integer, primary_key=True)
    currency = Column(String(255)
    coin = Column(String(255)
    amount  = Column(Float)
    tx_dir = Column(String(255)
    tx_id = Column(String(255)
    wallet_type = Column(String(255) #Set choices between wallet types: Exchange/Margin
    timestamp = Column(DateTime)
    userId = Column(ForeignKey, 'user.id')
    
    def __init__(self, currency, coin, amount, tx_dir, tx_id, wallet_type):
        self.currency, self.coin, self.amount, self.tx_dir, self.tx_id, self.wallet_type = currency, amount, tx_dir, tx_id, wallet_type
        self.timestamp = datetime.utc_now()
       
    def save(self):
        try:
            db.add(self)
            db.commit()
        except Exception as e:
            logging.debug(e)
            db.rollback()
    
        
    
    
    
    
    
